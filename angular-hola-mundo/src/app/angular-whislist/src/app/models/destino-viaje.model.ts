export class DestinoViaje {
    private selected!: boolean;
    servicios: string[];

constructor( public nombre:string, public imagenUrl:string) {
    this.servicios = ['Pileta', 'Desayuno'];
}
isSelected(): boolean {
    return this.selected;
    }
    setSelected (s: boolean){
        this.selected = s;
    }
}