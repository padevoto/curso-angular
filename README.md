Introducción curso de angular
Gratis

56 minutos


Aprender que es Angular, cuales son sus beneficios y saber que requerimientos técnicos debemos tener en cuenta antes de comenzar.

1.1 - Introducción a Angular y preview ejercicio a realizar

01:21
1.2 - ¿Qué es Angular?

03:48
1.3 - Requisitos necesarios para comenzar (html, css, typescript)

01:40
1.4 - Preparar entorno de trabajo para comenzar a crear nuestra aplicación

09:24
1.5 - Iniciar aplicación en Angular

07:01
1.6 - Explicar estructura de carpetas por defecto

14:29
1.7 - ¿Qué es un modulo? Generando el primer modulo

08:50
1.8 - ¿Qué es un componente? Generando el primer componente

08:37